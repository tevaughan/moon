{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Launch from the Moon via Rail-Car\n",
    "\n",
    "Thomas E. Vaughan, 2019 June\n",
    "\n",
    "Below is an analysis of a system that propels at constant acceleration a magnetically levitated car, on a railway several kilometers long across the surface of the Moon, until the car reaches a speed a bit greater than the orbital speed around the Moon at the surface."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Definitions\n",
    "\n",
    "Before discussing what I desire for a system that launches a car from the Moon, I might define some terms.\n",
    "\n",
    "- For an orbit around the Moon, *periselene* is the point of the orbit's closest approach to the Moon.\n",
    "\n",
    "- *Apselene* is the point of the orbit's farthest retreat from the Moon.\n",
    "\n",
    "- The *altitude at apselene* is the altitude, above the surface of the Moon, to which the car will rise as it approaches the side of the Moon opposite from the site of the railway.\n",
    "\n",
    "## Desiderata\n",
    "\n",
    "- An altitude of 425 km is a good target for apselene because then a circularising burn at apselene would place the car into an orbit that is in the middle of the range of stable altitudes.  Above 100 km, the orbit avoids substantial effects of gravitational perturbation from inhomogeneities in the Moon's density. (See, for example, https://en.wikipedia.org/wiki/Lunar_orbit.)  Below 750 km, the orbit avoids substantial perturbations from the Earth's gravity.  (See, for example, https://science.nasa.gov/science-news/science-at-nasa/2006/30nov_highorbit.)\n",
    "\n",
    "- The acceleration of the car on the railway is chosen to be about three gees.  This would be tolerable for almost all human passengers.\n",
    "\n",
    "- The mass of the car (together with what's inside it) is chosen to be about 200,000 kg.  That is a bit more mass than what SpaceX's Super Heavy system is supposed to lift to LEO from the surface of the Earth.  This would be enough mass so that, easily, a couple of hundred persons could be launched to rendezvous in orbit with a fueled stage that could return to Earth or go elsewhere.\n",
    "\n",
    "- The time to charge the rail-launch system is chosen to be about an hour.\n",
    "\n",
    "The basic idea is to enable a large number of passengers and cargo to come and go from the Moon while ensuring that fuel required to leave the Moon need never be brought down to the bottom of the Moon's gravity well. The bulk of the fuel required to leave the Moon need only be brought down to a circular orbit whose altitude is that of the launched car at apselene.  At launch, the car would need to carry only enough fuel to circularize its orbit.  The car would be fueled at rendezvous in orbit so that it could return to a point near the initial terminus of the railway on the surface of the Moon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pint  # for physical units\n",
    "u = pint.UnitRegistry()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "alt_ap      = 425*u.km        # altitude at apselene\n",
    "accel       = 30*u.m/u.s/u.s  # acceleration on railway\n",
    "mass        = 200000*u.kg     # mass launched\n",
    "charge_time = 60*u.minute     # time to charge for launch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background\n",
    "\n",
    "In 2017, Ross Pomeroy wrote for *Space.com* a nice [review](https://www.space.com/38384-could-startram-revolutionize-space-travel.html) of what has been called \"StarTram\" by its inventors, James R. Powell (deceased, 2019 May) and George Maise.  See also the [article on StarTram at Wikipedia](https://en.wikipedia.org/wiki/StarTram).\n",
    "\n",
    "The proposal for StarTram includes a Generation-1 system that would launch only cargo from the surface of the Earth into orbit around the Earth and a Generation-2 system that would launch passengers.\n",
    "\n",
    "- The Generation-1 system, which would accelerate a car to orbital velocity in an evacuated tube that runs up the side of a mountain, seems possible to build, but it could not carry passengers because both the acceleration in the tube and the deceleration when the car hits the atmosphere around an altitude of 5 km would be too difficult for human beings to withstand.\n",
    "\n",
    "- The Generation-2 system, which would levitate not only the car within the tube but also the tube itself, by repelling the Earth's gravitational field, so that the exit-terminus of the tube would lie at an altitude of 20 km, seems to me completely impractical.\n",
    "\n",
    "Although all of the technology required for Generation 2 already exists (for example, superconducting, niobium-titanium cables that can carry enough current to levitate the tube against the Earth's magnetic field), I don't see any way to make the system reliable and safe, mainly because of the required cooling to liquid-helium temperatures of hundreds of miles of cabling, most of which reaches ten miles into the sky.\n",
    "\n",
    "Nevertheless, on the Moon, a system even simpler than Generation 1 of Star Tram could economically lift large payloads of cargo and passengers from the surface of the Moon."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis\n",
    "\n",
    "### Terminal Speed\n",
    "\n",
    "The terminal speed at the end of the railway is enough for the car to rise to the desired altitude at apselene, above the side of the Moon opposite from the site of the railway."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "M_selene = 7.35E+22*u.kg  # approximate mass     of the Moon\n",
    "D_selene = 3479*u.km      # approximate diameter of the Moon\n",
    "R_selene = D_selene/2.0   # approximate radius   of the Moon"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = u.newtonian_constant_of_gravitation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "r_p = R_selene           # distance from focus at periselene\n",
    "r_a = R_selene + alt_ap  # distance from focus at   apselene"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "alpha = 0.5*(r_p + r_a)  # semi-major axis of orbit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.77e+03 meter / second\n"
     ]
    }
   ],
   "source": [
    "import numpy\n",
    "# launch-speed from periselene (terminal speed after acceleration)\n",
    "v_p = numpy.sqrt(G*M_selene*(2.0/r_p - 1.0/alpha))\n",
    "print(format(v_p.to('m/s'), \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Length of Rail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "52.1 kilometer\n"
     ]
    }
   ],
   "source": [
    "length = v_p*v_p/2.0/accel\n",
    "print(format(length.to('km'), \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time Spent Accelerating on Rail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "58.9 second\n"
     ]
    }
   ],
   "source": [
    "time = v_p/accel\n",
    "print(format(time.to('s'), \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Energy Required for Launch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "313 gigajoule\n"
     ]
    }
   ],
   "source": [
    "energy = 0.5*mass*v_p*v_p\n",
    "print(format(energy.to('GJ'), \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fraction of Escape-Energy Applied by Rail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.554 dimensionless\n"
     ]
    }
   ],
   "source": [
    "esc_energy = G*M_selene*mass/r_p\n",
    "print(format(energy/esc_energy, \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Power Required for Launch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.3 gigawatt\n"
     ]
    }
   ],
   "source": [
    "power_mean = energy/time\n",
    "print(format(power_mean.to('GW'), \".3g\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "10.6 gigawatt\n"
     ]
    }
   ],
   "source": [
    "power_peak = 2.0*power_mean  # power just before release\n",
    "print(format(power_peak.to('GW'), \".3g\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Area of Solar Panels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "cell_efficiency = 0.2  # assumed net efficiency for solar panels\n",
    "prop_efficiency = 0.2  # assumed net efficiency of propulsion\n",
    "solar_constant  = 1360*u.W/u.m/u.m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.6 kilometer ** 2\n"
     ]
    }
   ],
   "source": [
    "area = energy/solar_constant/charge_time/cell_efficiency/prop_efficiency\n",
    "print(format(area.to('km^2'), \".3g\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "30.6 meter\n"
     ]
    }
   ],
   "source": [
    "mean_width = area/length\n",
    "print(format(mean_width.to('m'), \".3g\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
